package com.poroshyn.voter.main;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.os.AsyncTask;
import java.io.IOException;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import java.util.ArrayList;
import java.util.List;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.protocol.HTTP;

import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.poroshyn.voter.R;
import com.poroshyn.voter.connect.ConnectActivity;

public class MainActivity extends AppCompatActivity {

    String SERVER_URL ;
    String SERVER_TITLE = "Otv1";
    String SERVER_MESSAGE = "Otv2";
    Button ButtonYes, ButtonNo;
    String Otv1, Otv2;
    String ip, id;
    ProgressBar pb;
    int start, timeout;
    TextView timer;
    boolean debug = false;
    int second;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButtonYes = (Button) findViewById(R.id.button2);
        ButtonNo = (Button) findViewById(R.id.button);
        pb = (ProgressBar) findViewById(R.id.progressBar1);
        pb.setVisibility(View.GONE);
        timer = (TextView) findViewById(R.id.timer1);
        timer.setVisibility(View.INVISIBLE);

        start = Integer.parseInt(getIntent().getStringExtra("start"));
        timeout = Integer.parseInt(getIntent().getStringExtra("timeout"));
        ip = getIntent().getStringExtra("ip");
        id = getIntent().getStringExtra("id");
        SERVER_URL = "http://"+ip+"/"+id+".php";

        Switch sw = (Switch) findViewById(R.id.switch1);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    debug = true;
                } else {
                    debug = false;
                }
            }
        });

        // String  titleToSend = titleTextField.getText().toString();
        ButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                // TODO Auto-generated method stub
                Otv1 = "Yes";
                Otv2 = "No";
                pb.setVisibility(View.VISIBLE);
                new MyAsyncTask().execute(Otv1, Otv2);

                if(debug == false) {
                    ButtonYes.setVisibility(View.INVISIBLE);
                    ButtonNo.setVisibility(View.INVISIBLE);
                    timer.setVisibility(View.VISIBLE);

                    ButtonYes.setVisibility(View.INVISIBLE);
                    ButtonNo.setVisibility(View.INVISIBLE);
                    timer.setVisibility(View.VISIBLE);

                    new CountDownTimer(timeout*1000, 1000) {

                        //Здесь обновляем текст счетчика обратного отсчета с каждой секундой
                        public void onTick(long millisUntilFinished) {
                            //second = (int)millisUntilFinished/1000;
                            timer.setText("Осталось: "
                                    + millisUntilFinished / 1000);;
                        }
                        //Задаем действия после завершения отсчета (высвечиваем надпись "Бабах!"):
                        public void onFinish() {
                            ButtonYes.setVisibility(View.VISIBLE);
                            ButtonNo.setVisibility(View.VISIBLE);
                            timer.setVisibility(View.INVISIBLE);
                        }
                    }.start();

                }
            }

        });

        ButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                // TODO Auto-generated method stub
                Otv1 = "No";
                Otv2 = "Yes";
                pb.setVisibility(View.VISIBLE);
                new MyAsyncTask().execute(Otv1, Otv2);

                if(debug == false) {
                    ButtonYes.setVisibility(View.INVISIBLE);
                    ButtonNo.setVisibility(View.INVISIBLE);
                    timer.setVisibility(View.VISIBLE);

                    new CountDownTimer(timeout*1000, 1000) {

                        //Здесь обновляем текст счетчика обратного отсчета с каждой секундой
                        public void onTick(long millisUntilFinished) {
                            //second = (int)millisUntilFinished/1000;
                            timer.setText("Осталось: "
                                    + millisUntilFinished / 1000);;
                        }
                        //Задаем действия после завершения отсчета (высвечиваем надпись "Бабах!"):
                        public void onFinish() {
                            ButtonYes.setVisibility(View.VISIBLE);
                            ButtonNo.setVisibility(View.VISIBLE);
                            timer.setVisibility(View.INVISIBLE);
                        }
                    }.start();
                }

            }

        });

       new CountDownTimer(start*1000+100, 1000) {

            //Здесь обновляем текст счетчика обратного отсчета с каждой секундой
            public void onTick(long millisUntilFinished) {

            }
            //Задаем действия после завершения отсчета (высвечиваем надпись "Бабах!"):
            public void onFinish() {

                Toast.makeText(getApplicationContext(), "Finish", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, ConnectActivity.class);
                startActivity(intent);

            }
        }.start();


    }


    private class MyAsyncTask extends AsyncTask<String, Integer, Double> {
        @Override

        protected Double doInBackground(String... params) {
            // TODO Auto-generated method stub
            postData(params[0], params[1]);
            return null;
        }

        protected void onPostExecute(Double result) {
            pb.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "Info sent", Toast.LENGTH_LONG).show();
            //title_param.setText(""); //reset the message text field title_param, msg_param;
            //msg_param.setText("");
        }
        protected void onProgressUpdate(Integer... progress) {
            pb.setProgress(progress[0]);
        }

        public void postData(String titleToSend, String msgToSend) {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(SERVER_URL);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair(SERVER_TITLE, titleToSend));
                nameValuePairs.add(new BasicNameValuePair(SERVER_MESSAGE, msgToSend));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                Toast.makeText(getBaseContext(), "Client Protocol Exception", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Toast.makeText(getBaseContext(), "IO Exception", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
