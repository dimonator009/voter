package com.poroshyn.voter.connect;

public interface ConnectView {

    void showProgress();

    void hideProgress();

    void setIpError();

    void setIdError();

    void setConnectionError();

    void setSessionError();

    void navigateToHome(String timeout, String start);

}
