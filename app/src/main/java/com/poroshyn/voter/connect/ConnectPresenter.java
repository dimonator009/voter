package com.poroshyn.voter.connect;


public class ConnectPresenter implements ConnectInteractor.OnConnectFinishedListener{

    private ConnectView ConnectView;
    private ConnectInteractor ConnectInteractor;


    ConnectPresenter(ConnectView ConnectView,ConnectInteractor ConnectInteractor) {
        this.ConnectView = ConnectView;
        this.ConnectInteractor = ConnectInteractor;
    }

    public void validateCredentials(String server, String id_session){
       if (ConnectView != null) {
            ConnectView.showProgress();
        }

        ConnectInteractor.connect(server, id_session, this) ;
    }

    public void onDestroy() {
        ConnectView = null;
    }

    @Override
    public void onIpError() {
        if (ConnectView != null) {
            ConnectView.setIpError();
            ConnectView.hideProgress();
        }
    }

    @Override
    public void onIdError() {
        if (ConnectView != null) {
            ConnectView.setIdError();
            ConnectView.hideProgress();
        }
    }

    @Override
    public void onConnectionError() {
        if (ConnectView != null) {
            ConnectView.setConnectionError();
            ConnectView.hideProgress();
        }
    }

    @Override
    public void onSessionError() {
        if (ConnectView != null) {
            ConnectView.setSessionError();
            ConnectView.hideProgress();
        }
    }

    @Override
    public void onSuccess(String timeout, String start) {
        if (ConnectView != null) {
            ConnectView.hideProgress();
            ConnectView.navigateToHome(timeout, start);
        }
    }

}
