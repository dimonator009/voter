package com.poroshyn.voter.connect;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class ConnectInteractor {

    interface OnConnectFinishedListener {

        void onIpError();

        void onIdError();

        void onSuccess(String timeout, String start);

        void onConnectionError();

        void onSessionError();


    }

    public void connect(final String server, final String id_session, final OnConnectFinishedListener listener) {

        if (TextUtils.isEmpty(server)) {
            listener.onIpError();
            return;
        }
        if (TextUtils.isEmpty(id_session)) {
            listener.onIdError();
            return;
        }

        String url1 = "http://" + server + "/" + id_session + "t2.php";
        String url2 = "http://" + server + "/" + id_session + "t3.php";

        final String[] answer = new String[2];

        @SuppressLint("StaticFieldLeak")
        class Get_time extends AsyncTask<String, String, Void> {


            protected Void doInBackground(String... urls) {
                try {
                    int counter = 0;
                    for (String url : urls) {

                        answer[counter] = downloadFile(url);
                        ++counter;

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute (Void result){
                // TODO: check this.exception
                // TODO: do something with the feed
                if(answer[0] == null || answer[1] == null){
                    listener.onConnectionError();
                }else if(!isSessionFound(answer[0])){
                    listener.onSessionError();
                }else {
                    listener.onSuccess(answer[0], answer[1]);
                }
            }

            private String downloadFile(String url) throws IOException {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                try (Response response = client.newCall(request).execute()) {
                    return Objects.requireNonNull(response.body()).string();
                }
            }

            private boolean isSessionFound (String value){
                try{
                    Integer.parseInt(value.replaceAll("\n|\r\n", ""));
                    return true;
                }catch (Exception e){
                    e.printStackTrace();
                    return false;
                }
            }


        }

        Get_time get_time = new Get_time();
        get_time.execute(url1, url2);


    }

}
