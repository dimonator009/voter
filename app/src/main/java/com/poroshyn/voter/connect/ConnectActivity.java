package com.poroshyn.voter.connect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.poroshyn.voter.R;
import com.poroshyn.voter.main.MainActivity;


public class ConnectActivity extends AppCompatActivity implements ConnectView{

    private EditText id_session;
    private EditText server;
    private ProgressBar progressBar;
    private ConnectPresenter presenter;
    private Button connectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        id_session = findViewById(R.id.editText2);
        server = findViewById(R.id.editText);
        progressBar = findViewById(R.id.progress);
        connectButton = findViewById(R.id.button3);

        findViewById(R.id.button3).setOnClickListener(v -> validateCredentials());

        presenter = new ConnectPresenter(this, new ConnectInteractor());


    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        connectButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        connectButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void setIpError() {
        server.setError(getString(R.string.ip_error));
    }

    @Override
    public void setIdError() {
        id_session.setError(getString(R.string.id_error));
    }

    @Override
    public void setConnectionError() {
        Toast.makeText(getBaseContext(), getString(R.string.error_connection) , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSessionError() {
        Toast.makeText(getBaseContext(), getString(R.string.error_session) , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToHome(String timeout, String start) {

        //start = start.replaceAll("\n|\r\n", "");
       //timeout = timeout.replaceAll("\n|\r\n", "");
        Toast.makeText(getBaseContext(), timeout+ " - " + start, Toast.LENGTH_SHORT).show();
        /*Intent intent = new Intent(ConnectActivity.this, MainActivity.class);
        intent.putExtra("start", start.replaceAll("\n|\r\n", ""));
        intent.putExtra("timeout", timeout.replaceAll("\n|\r\n", ""));
        intent.putExtra("ip", server.getText().toString());
        intent.putExtra("id", id_session.getText().toString());
        startActivity(intent);
        finish();*/
    }

    private void validateCredentials() {
        presenter.validateCredentials(server.getText().toString(), id_session.getText().toString());
    }






}
